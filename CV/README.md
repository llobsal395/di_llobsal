# Curriculum Vitae - Lucía Lobato

| HOLA

![alt text](/CV/img/foto.webp "Hombre mirando PC intensamente"){width=200 height=200px}

*Alumna de 2º DAM en **IES Velazquez***

Contacto: [llobsal395@g.educaand.es](llobsal395@g.educaand.es)

## Experiencia Profesional

- NASA (1993 - 20030)
  - Directora
  - Elegir muebles
- Pizzeria Paco (2000 - 2040)
  - Cantautora en el local
    - Cobraba según las propinas
    - A veces eran caramelos
  - Limpiar platos

## Títulos Académicos

1. Título de Grado Medio Farmacia y Parafarmacia (2018 - 2020)
2. Título ESO (Pruebas libres 2019)
3. Diploma de "El inglés se enseña mal" (2010)
4. Título de CryptoMonedas School impartido por un Crosffiestero (2014)

| Habilidad   |      Descripción      |  Experiencia |
|---------:|:-------------:|------:|
| Java |  Lenguaje de programación multiplataforma orientados a objetos | :star: :star: :star: |
| HTML |    lenguaje de marcado para la elaboración de páginas web   |   :star: :star: :star: :star: :star:|
| CSS | lenguaje de diseño gráfico para definir y crear la presentación de un documento estructurado escrito en un lenguaje de marcado | :star: :star: :star: :star: |

## Tarjeta de Visita

```ruby
require 'redcarpet'
markdown = Redcarpet.new("No se que hacer")
puts markdown.to_html
```